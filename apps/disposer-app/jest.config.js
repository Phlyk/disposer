const uncompilingModules = ["@ionic-native/.*"].join("|");

module.exports = {
    /// ***** BASE CONFIG ****** ///
    // name to be displayed
    displayName: "disposer-app",
    // A preset that is used as a base for Jest's configuration
    preset: "../../jest.preset.js",
    // A list of paths to modules that run some code to configure or set up the testing framework before each test
    setupFilesAfterEnv: ["<rootDir>/src/test/test-setup.ts"],
    // ?
    globals: {
        "ts-jest": {
            tsConfig: "<rootDir>/tsconfig.spec.json",
            stringifyContentPathRegex: "\\.(html|svg)$",
            astTransformers: {
                before: ["jest-preset-angular/build/InlineFilesTransformer", "jest-preset-angular/build/StripStylesTransformer"],
            },
        },
    },
    // //// ********* COVERAGE ***** ////
    // // Indicates whether the coverage information should be collected while executing the test
    // collectCoverage: false,
    // // An array of glob patterns indicating a set of files for which coverage information should be collected
    // // collectCoverageFrom: undefined,
    // // The directory where Jest should output its coverage files
    // // An array of regexp pattern strings used to skip coverage collection
    // coveragePathIgnorePatterns: ["/node_modules/"],
    // // A list of reporter names that Jest uses when writing coverage reports
    coverageReporters: ["html", "text"], //"json","lcov","clover"

    coverageDirectory: "../../coverage/apps/disposer-app",

    snapshotSerializers: [
        "jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js",
        "jest-preset-angular/build/AngularSnapshotSerializer.js",
        "jest-preset-angular/build/HTMLCommentSerializer.js",
    ],

    // // A list of paths to directories that Jest should use to search for files in
    roots: ["<rootDir>/src"],
    // // The glob patterns Jest uses to detect test files
    // testMatch: ["**/+(*.)+(spec).+(ts)"],
    // // An array of regexp pattern strings that are matched against all test paths, matched tests are skipped
    testPathIgnorePatterns: ["node_modules", "<rootDir>/src/test/test-setup.ts"],
    // // A map from regular expressions to module names that allow to stub out resources with a single module
    moduleNameMapper: {
        "apps/disposer-app/src/(.*)": "<rootDir>/src/$1",
    },

    // //// *********** MOCKS ********** ////
    // // Automatically clear mock calls and instances between every test
    clearMocks: true,

    // // An array of regexp pattern strings that are matched against all source file paths, matched files will skip transformation
    transformIgnorePatterns: [
        `node_modules/(?!(${uncompilingModules}))`,
    ],
};
