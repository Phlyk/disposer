import { DLocationDTO, HandledWasteDTO, HandlerDTO, MaterialDTO } from "@disposer/api-interfaces";
import { DCoordinates } from "@disposer/common";
import { Geoposition } from "@ionic-native/geolocation/ngx";
import { HandlerFactory } from "apps/disposer-app/src/app/modules/handlers/model/handler.factory";
import { Handler } from "apps/disposer-app/src/app/modules/handlers/model/handler.model";
import { MaterialsService } from "apps/disposer-app/src/app/modules/materials/services/materials.service";
import { LocationService } from "apps/disposer-app/src/app/shared/services/location.service";
import { of } from "rxjs";
import "../../../utils/matchers/array-contains.matcher";
import { dummyTestMaterials } from "../_data/dummy-materials";

jest.mock("apps/disposer-app/src/app/shared/services/location.service");
jest.mock("apps/disposer-app/src/app/modules/materials/services/materials.service");
jest.mock("@disposer/api-interfaces");
const DUMMY_DISTANCE = 2;
const DUMMY_HANDLER_LATITUDE = 10;
const DUMMY_HANDLER_LONGITUDE = 10;
const DUMMY_USER_LATITUDE = 2;
const DUMMY_USER_LONGITUDE = 1;

describe("HandlerFactory (isolated test)", () => {
    let sut: HandlerFactory;

    let testHandlerDto: jest.Mocked<HandlerDTO>;

    let mockedLocationService: jest.Mocked<LocationService>;
    let mockedMaterialsService: jest.Mocked<MaterialsService>;
    const dummyMaterials: MaterialDTO[] = dummyTestMaterials;

    beforeEach(() => {
        mockedLocationService = new LocationService(null, null) as any;
        mockedLocationService.getLocation.mockImplementation(() =>
            Promise.resolve({ coords: { latitude: DUMMY_USER_LATITUDE, longitude: DUMMY_USER_LONGITUDE } } as Geoposition)
        );
        mockedLocationService.getDistanceBetween.mockImplementation((_a, _b) => ({ distance: DUMMY_DISTANCE, unit: "km" }));

        mockedMaterialsService = new MaterialsService(null, null) as any;
        mockedMaterialsService.fetchMaterials.mockImplementation(() => of(dummyMaterials));

        sut = new HandlerFactory(mockedMaterialsService, mockedLocationService);
    });

    test("should calculate distance between user's location and handler on creation", () => {
        setUpTestHandlerDto();

        const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);

        expect(createdHandler.distanceFromUser.distance).toEqual(DUMMY_DISTANCE);
        const userCoords: DCoordinates = { lat: DUMMY_USER_LATITUDE, long: DUMMY_USER_LONGITUDE };
        const handlerCoords: DCoordinates = { lat: DUMMY_HANDLER_LATITUDE, long: DUMMY_HANDLER_LONGITUDE };
        expect(mockedLocationService.getDistanceBetween).toHaveBeenCalledWith(userCoords, handlerCoords, "km");
    });

    test("should identify when a handler's accepted materials is unknown", () => {
        setUpTestHandlerDto();

        expect(testHandlerDto.handledWaste).toBeUndefined();
        expect(testHandlerDto.rejectedWaste).toBeUndefined();

        const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
        expect(createdHandler.handledMaterialIds).toEqual([]);
    });

    test("should identify when a handler accepts all materials", () => {
        const handledWaste: HandledWasteDTO = null;
        const rejectedWaste: HandledWasteDTO = { description: "we accept it all, and we'll prove it!" };

        setUpTestHandlerDto(handledWaste, rejectedWaste);

        const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
        expect(createdHandler.handledMaterialIds).toEqual(dummyMaterials.map(material => material._id));
    });

    describe("HandlerFactory accepted items logic categorisations", () => {
        test("should categorise one accepted item with no rejections", () => {
            const handledWaste: HandledWasteDTO = {
                items: [{ materialId: "plastic-bottles" }],
            };
            const rejectedWaste: HandledWasteDTO = null;

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqual(["plastic-bottles"]);
        });

        test("should categorise multiple accepted items with no rejections", () => {
            const handledWaste: HandledWasteDTO = {
                items: [{ materialId: "plastic-bottles" }, { materialId: "egg-cartons" }, { materialId: "centipede-fangs" }],
            };
            const rejectedWaste: HandledWasteDTO = null;

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqual(["plastic-bottles", "egg-cartons", "centipede-fangs"]);
        });

        test("should ignore rejected items when items are specified", () => {
            const handledWaste: HandledWasteDTO = {
                items: [{ materialId: "plastic-bottles" }, { materialId: "egg-cartons" }, { materialId: "centipede-fangs" }],
            };
            const rejectedWaste: HandledWasteDTO = {
                items: [{ materialId: "plastic-bottles" }],
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqual(["plastic-bottles", "egg-cartons", "centipede-fangs"]);
        });

        test("should ignore rejected groups when items are specified", () => {
            const handledWaste: HandledWasteDTO = {
                items: [{ materialId: "plastic-bottles" }, { materialId: "glass-container" }, { materialId: "centipede-fangs" }],
            };
            const rejectedWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable"] }, { categoryIds: ["bio-waste"] }],
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["plastic-bottles", "glass-container", "centipede-fangs"]);
        });
    });

    describe("HandlerFactory one accepted group logic categorisations", () => {
        test("should categorise one accepted group with no rejections", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable"] }],
                description: "We only accept the disposable stuff plz",
            };
            const rejectedWaste: HandledWasteDTO = null;

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray([
                "plastic-bottles",
                "glass-container",
                "egg-cartons",
                "supreme-waste",
            ]);
        });

        test("should categorise one accepted group with one item rejection", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable"] }],
                description: "We only accept the disposable stuff plz",
            };
            const rejectedWaste: HandledWasteDTO = {
                items: [{ materialId: "glass-container" }],
                description: "that shit is sharp man",
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["plastic-bottles", "egg-cartons", "supreme-waste"]);
        });

        test("should categorise one accepted group with multiple item rejections", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable"] }],
                description: "We only accept the disposable stuff plz",
            };
            const rejectedWaste: HandledWasteDTO = {
                items: [{ materialId: "glass-container" }, { materialId: "egg-cartons" }],
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["plastic-bottles", "supreme-waste"]);
        });

        test("should categorise one accepted group with single group rejection", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable"] }],
                description: "We only accept the disposable stuff plz",
            };
            const rejectedWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["glass"] }],
                description: "need specialist shiz for handling glass, plz no",
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["plastic-bottles", "egg-cartons", "supreme-waste"]);
        });

        test("should categorise one accepted group with multiple group rejections (inclusive)", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable"] }],
                description: "We only accept the disposable stuff plz",
            };
            const rejectedWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["glass"] }, { categoryIds: ["plastic"] }, { categoryIds: ["household"] }],
                description: "need specialist shiz for handling glass, plz no",
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["egg-cartons"]);
        });

        test("should categorise one accepted group with composite group rejections", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["bio-waste"] }],
                description: "We only accept the disposable stuff plz",
            };
            const rejectedWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["liquids-chemicals", "hazardous"] }],
                description: "We'll have all your non liquid, hazardous bio-waste",
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["centipede-fangs"]);
        });
    });

    describe("HandlerFactory multiple singular accepted groups logic categorisations", () => {
        test("should categorise multiple singular accepted groups with no rejections", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["bio-waste"] }, { categoryIds: ["plastic"] }],
            };
            const rejectedWaste: HandledWasteDTO = null;

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray([
                "plastic-bottles",
                "centipede-fangs",
                "doom",
                "supreme-waste",
            ]);
        });

        test("should categorise multiple singular accepted groups with one item rejection", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["electronics"] }, { categoryIds: ["glass"] }],
            };
            const rejectedWaste: HandledWasteDTO = {
                items: [{ materialId: "doom" }],
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["glass-container", "laptop"]);
        });

        test("should categorise multiple singular accepted groups with multiple item rejections", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable"] }, { categoryIds: ["plastic"] }, { categoryIds: ["hazardous"] }],
            };
            const rejectedWaste: HandledWasteDTO = {
                items: [{ materialId: "doom" }, { materialId: "egg-cartons" }, { materialId: "supreme-waste" }],
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["centipede-fangs", "plastic-bottles", "glass-container"]);
        });

        test("should categorise multiple singular accepted groups with single group rejections", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable"] }, { categoryIds: ["plastic"] }, { categoryIds: ["hazardous"] }],
            };
            const rejectedWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["bio-waste"] }],
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["plastic-bottles", "glass-container", "egg-cartons", "supreme-waste"]);
        });

        test("should categorise multiple singular accepted groups with multiple single-category group rejections (inclusive)", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable"] }, { categoryIds: ["plastic"] }, { categoryIds: ["hazardous"] }],
            };
            const rejectedWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["bio-waste"] }, { categoryIds: ["glass"] }],
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["plastic-bottles", "egg-cartons", "supreme-waste"]);
        });

        test("should categorise multiple singular accepted groups with composite group rejections", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["bio-waste"] }, { categoryIds: ["plastic"] }, { categoryIds: ["hazardous"] }],
            };
            const rejectedWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["bio-waste", "liquids-chemicals"] }],
            };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["plastic-bottles", "centipede-fangs", "supreme-waste"]);
        });
    });

    describe("HandlerFactory composite accepted groups logic categorisations", () => {
        test("should categorise one composite accepted group with no rejections", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable", "plastic"] }],
            };
            const rejectedWaste: HandledWasteDTO = null;

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["plastic-bottles", "supreme-waste"]);
        });

        test("should categorise one accepted composite group with one item rejection", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["bio-waste", "hazardous"] }],
            };
            const rejectedWaste: HandledWasteDTO = { items: [{ materialId: "doom" }] };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["centipede-fangs"]);
        });

        test("should categorise one accepted composite group with multiple item rejections", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["bio-waste", "hazardous"] }],
            };
            const rejectedWaste: HandledWasteDTO = { items: [{ materialId: "doom" }, { materialId: "centipede-fangs" }] };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray([]);
        });

        test("should categorise one accepted composite group with one single category group rejections", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["bio-waste", "hazardous"] }],
            };
            const rejectedWaste: HandledWasteDTO = { groups: [{ categoryIds: ["liquids-chemicals"] }] };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["centipede-fangs"]);
        });
        test("should categorise one accepted composite group with multiple group rejections (inclusive)", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable", "household"] }],
            };
            const rejectedWaste: HandledWasteDTO = { groups: [{ categoryIds: ["paint"] }, { categoryIds: ["plastic"] }] };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["glass-container"]);
        });

        test("should categorise one accepted composite group with composite group rejections", () => {
            const handledWaste: HandledWasteDTO = {
                groups: [{ categoryIds: ["consumable", "household"] }],
            };
            const rejectedWaste: HandledWasteDTO = { groups: [{ categoryIds: ["paint", "plastic"] }] };

            setUpTestHandlerDto(handledWaste, rejectedWaste);

            const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
            expect(createdHandler.handledMaterialIds).toEqualArray(["glass-container"]);
        });
    });

    test("should handle a super complex example", () => {
        const handledWaste: HandledWasteDTO = {
            groups: [
                { categoryIds: ["electronics"] },
                { categoryIds: ["consumable", "household"] }
            ],
            items: [{materialId: "centipede-fangs"}]
        };
        const rejectedWaste: HandledWasteDTO = { 
            groups: [{ categoryIds: ["paint", "plastic"] }],
            items: [{materialId: "laptop"}]
         };

        setUpTestHandlerDto(handledWaste, rejectedWaste);

        const createdHandler: Handler = sut.createHandlerFrom(testHandlerDto);
        expect(createdHandler.handledMaterialIds).toEqualArray(["centipede-fangs", "glass-container"]);
    });

    function setUpTestHandlerDto(handledWaste?: HandledWasteDTO, rejectedWaste?: HandledWasteDTO) {
        testHandlerDto = {} as any;
        testHandlerDto.name = "testHandler";
        testHandlerDto.location = {
            latitude: DUMMY_HANDLER_LATITUDE,
            longitude: DUMMY_HANDLER_LONGITUDE,
        } as DLocationDTO;
        if (handledWaste) {
            testHandlerDto.handledWaste = handledWaste;
        }
        if (rejectedWaste) {
            testHandlerDto.rejectedWaste = rejectedWaste;
        }
    }
});
