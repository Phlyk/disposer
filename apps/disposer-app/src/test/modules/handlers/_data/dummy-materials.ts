import { MaterialDTO } from "@disposer/api-interfaces";

const material1: MaterialDTO = {
    _id: "plastic-bottles",
    name: "Plastic Bottles",
    categories: [
        { _id: "plastic", name: "Plastic" },
        { _id: "consumable", name: "Consumable" },
    ],
};
const material3: MaterialDTO = {
    _id: "egg-cartons",
    name: "Egg Cartons",
    categories: [
        { _id: "carboard", name: "Cardboard" },
        { _id: "consumable", name: "Consumable" },
    ],
};
const material4: MaterialDTO = {
    _id: "centipede-fangs",
    name: "Centipede Fangs",
    categories: [
        { _id: "bio-waste", name: "Bio Waste" },
        { _id: "hazardous", name: "Hazardous" },
    ],
};
const material2: MaterialDTO = {
    _id: "doom",
    name: "Do0om",
    categories: [
        { _id: "bio-waste", name: "Bio Waste" },
        { _id: "hazardous", name: "Hazardous" },
        { _id: "liquids-chemicals", name: "Liquids & Chemicals" },
    ],
};
const material5: MaterialDTO = {
    _id: "laptop",
    name: "Laptop",
    categories: [{ _id: "electronics", name: "Electronics" }],
};
const material6: MaterialDTO = {
    _id: "glass-container",
    name: "Glass Container",
    categories: [
        { _id: "consumable", name: "Consumable" },
        { _id: "household", name: "Household" },
        { _id: "glass", name: "Glass" },
    ],
};
const someComplexClassifiedMaterial: MaterialDTO = {
    _id: "supreme-waste",
    name: "Supreme Waste",
    categories: [
        { _id: "consumable", name: "Consumable" },
        { _id: "household", name: "Household" },
        { _id: "paint", name: "Paint" },
        { _id: "plastic", name: "Plastic" },
    ],
};

export const dummyTestMaterials = [
    material1,
    material2,
    material3,
    material4,
    material5,
    material6,
    someComplexClassifiedMaterial,
];
