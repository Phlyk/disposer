export { };

declare global {
    namespace jest {
        interface Matchers<R> {
            toContainArray(expected: any[]): R;
            toEqualArray(expected: any[]): R;
        }
    }
}

expect.extend({
    toContainArray(received: any[], expected: any[]) {
        const pass = received.every(receivedItem => expected.includes(receivedItem));
        return {
            message: () => `expected ${this.utils.printReceived(received)} ${pass ? "not " : " "}to contain ${this.utils.printExpected(expected)}`,
            pass
        }
    },
});

expect.extend({
    toEqualArray(received: any[], expected: any[]) {
        const expectedString = this.utils.stringify(expected.sort());
        const receivedString = this.utils.stringify(received.sort());
        const pass = receivedString === expectedString;
        return {
            message: () => `expected ${this.utils.printReceived(received)} ${pass ? "not " : " "}to equal ${this.utils.printExpected(expected)}`,
            pass
        }
    },
});
