export class TestUtils {
    static readonly defaultWindowInnerWidth = 1000;

    static resetWindowInnerWidth() {
        TestUtils.setWindowInnerWidth(TestUtils.defaultWindowInnerWidth);
    }

    static setWindowInnerWidth(widthToSet: number) {
        Object.defineProperty(window, "innerWidth", {value: widthToSet, configurable: true});
    }
}
