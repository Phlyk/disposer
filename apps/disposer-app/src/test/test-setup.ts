import "jest-preset-angular";
import { TestUtils } from "./utils/test-utils";

Object.defineProperty(window, "innerWidth", { value: TestUtils.defaultWindowInnerWidth });
Object.defineProperty(window, "CSS", { value: null });
Object.defineProperty(document, "doctype", {
    value: "<!DOCTYPE html>",
});

Error.stackTraceLimit = 4;
