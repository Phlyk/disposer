import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "../../shared/shared.module";
import { MapPage } from "./pages/map/map.page";

@NgModule({
    declarations: [MapPage],
    imports: [
        CommonModule,
        SharedModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: "",
                component: MapPage,
            },
        ]),
    ],
})
export class MapModule {}
