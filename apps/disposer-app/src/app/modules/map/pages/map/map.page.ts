import { AfterViewInit, Component, ElementRef, ViewChild } from "@angular/core";
import { LocationService } from "apps/disposer-app/src/app/shared/services/location.service";
import { Handler } from "../../../handlers/model/handler.model";
import { HandlerService } from "../../../handlers/services/handler.service";

@Component({
    selector: "disposer-map",
    templateUrl: "./map.page.html",
    styleUrls: ["./map.page.scss"],
})
export class MapPage implements AfterViewInit {
    @ViewChild("mapCanvas") mapElement: ElementRef;
    map: google.maps.Map;
    readonly iconBaseUrl = "https://developers.google.com/maps/documentation/javascript/examples/full/images/";

    constructor(private locationService: LocationService, private handlersService: HandlerService) {}

    ngAfterViewInit(): void {
        this.initMap();
    }

    async initMap() {
        const currentLocation = await this.locationService.getLocation();
        this.handlersService.fetchHandlers().subscribe(handlers => handlers.forEach(handler => this.processHandler(handler)));

        const currentLocationCoords = new google.maps.LatLng(currentLocation.coords.latitude, currentLocation.coords.longitude);
        const mapOptions: google.maps.MapOptions = {
            center: currentLocationCoords,
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        const yourLocationMarker = new google.maps.Marker({
            map: this.map,
            draggable: true,
            position: currentLocationCoords,
        });
    }

    private processHandler(handler: Handler) {
        const handlerLocation = new google.maps.LatLng(
            handler.dto.location.latitude,
            handler.dto.location.longitude
        );
        const handlerMarker = new google.maps.Marker({
            map: this.map,
            position: handlerLocation,
            icon: `${this.iconBaseUrl}info-i_maps.png` //${handlerType}`(""),
        });
        const handlerInfoWindow = new google.maps.InfoWindow({
            maxWidth: 400,
            content: `
            <div class="style-my-container">
                <h3>${handler.dto.name}</h3>
                <div class="style-me">${handler.dto.location.address}</div>
                <ion-button href="handlers/${handler.dto._id}">Go to handler page</ion-button>
            </div>
            `,
        });
        handlerMarker.addListener("click", () => handlerInfoWindow.open(this.map, handlerMarker));
    }
}
