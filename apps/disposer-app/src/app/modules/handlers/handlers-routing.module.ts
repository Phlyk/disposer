import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HandlerPage } from "./pages/handler/handler.page";
import { HandlersPage } from "./pages/handlers/handlers.page";

const routes: Routes = [
    {
        path: "",
        component: HandlersPage,
    },
    {
        path: ":handlerId",
        component: HandlerPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HandlersRoutingModule {}
