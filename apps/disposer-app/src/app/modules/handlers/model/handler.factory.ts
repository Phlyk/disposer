import { Injectable } from "@angular/core";
import { HandledWasteGroupDTO, HandledWasteMaterialDTO, HandlerDTO, MaterialDTO } from "@disposer/api-interfaces";
import { DCoordinates, Distance } from "@disposer/common";
import { Geoposition } from "@ionic-native/geolocation/ngx";
import { BehaviorSubject, from, zip } from "rxjs";
import { LocationService } from "../../../shared/services/location.service";
import { MaterialsService } from "../../materials/services/materials.service";
import { Handler } from "./handler.model";

@Injectable({
    providedIn: "root",
})
export class HandlerFactory {
    allMaterials: MaterialDTO[];
    ready$ = new BehaviorSubject<boolean>(false);
    private currentLocation: Geoposition;

    constructor(private materialsService: MaterialsService, private locationService: LocationService) {
        this.locationService
            .getLocation()
            .then(position => (this.currentLocation = position))
            .catch((this.currentLocation = null));
        const location$ = from(this.locationService.getLocation())
        zip(this.materialsService.fetchMaterials(), location$).subscribe(([materials, position]: [MaterialDTO[], Geoposition]) => {
            this.allMaterials = materials;
            this.currentLocation = position;
            this.ready$.next(true);
        });
    }

    createHandlerFrom(handlerDto: HandlerDTO): Handler {
        const handlerHandledMaterials = this.calculateHandlerMaterialIds(handlerDto);
        const distanceFromUser = this.getDistanceBetween(handlerDto);
        return new Handler(handlerDto, handlerHandledMaterials, distanceFromUser);
    }

    private getDistanceBetween(handlerDto: HandlerDTO): Distance {
        const currentCoords: DCoordinates = {
            lat: this.currentLocation.coords.latitude,
            long: this.currentLocation.coords.longitude,
        };
        const handlerCoords: DCoordinates = { lat: handlerDto.location.latitude, long: handlerDto.location.longitude };
        const distance = this.locationService.getDistanceBetween(currentCoords, handlerCoords, "km");
        return distance;
    }

    private calculateHandlerMaterialIds(handlerDto: HandlerDTO): string[] {
        const handledMaterialIds: string[] = [];
        if (this.handlerAcceptedMatsUnknown(handlerDto)) {
            return handledMaterialIds;
        }
        if (this.handlerAcceptsAllMats(handlerDto)) {
            handledMaterialIds.push(...this.allMaterials.map(material => material._id));
            return handledMaterialIds;
        }
        const acceptedMaterialIds = this.allMaterials
            .filter(material => this.handlerAcceptsMat(handlerDto, material))
            .map(acceptedMaterial => acceptedMaterial._id);
        handledMaterialIds.push(...acceptedMaterialIds);

        return handledMaterialIds;
    }

    private handlerAcceptedMatsUnknown(handlerDto: HandlerDTO): boolean {
        return !handlerDto.handledWaste && !handlerDto.rejectedWaste;
    }

    private handlerAcceptsAllMats(handlerDto: HandlerDTO): boolean {
        return !handlerDto.handledWaste && !handlerDto.rejectedWaste?.groups && !handlerDto.rejectedWaste?.items;
    }

    private handlerAcceptsMat(handlerDto: HandlerDTO, material: MaterialDTO): boolean {
        let isAccepted: boolean;
        if (handlerDto.handledWaste) {
            if (this.isMaterialIn(handlerDto.handledWaste.items, material)) {
                return true;
            }

            if (this.isMaterialInGroup(handlerDto.handledWaste.groups, material)) {
                isAccepted = true;
            }
        }
        if (handlerDto.rejectedWaste) {
            if (this.isMaterialIn(handlerDto.rejectedWaste.items, material)) {
                isAccepted = false;
            }

            if (this.isMaterialInGroup(handlerDto.rejectedWaste.groups, material)) {
                isAccepted = false;
            }
        }
        return isAccepted;
    }

    private isMaterialIn(wasteItems: HandledWasteMaterialDTO[], material: MaterialDTO): boolean {
        if (!wasteItems || !wasteItems.length) {
            return false;
        }
        return !!wasteItems.find(item => item.materialId === material._id);
    }

    private isMaterialInGroup(wasteGroups: HandledWasteGroupDTO[], material: MaterialDTO): boolean {
        if (!wasteGroups || !wasteGroups.length) {
            return false;
        }
        return wasteGroups.some(wasteGroup => {
            if (wasteGroup.categoryIds.length == 1) {
                return material.categories.find(category => category._id === wasteGroup.categoryIds[0]);
            }

            if (wasteGroup.categoryIds.length > 1) {
                const handledMaterialsForCategories = this.getMaterialsForCombinedCategories(wasteGroup.categoryIds);
                return !!handledMaterialsForCategories.find(handledMaterial => handledMaterial._id == material._id);
            }
        });
    }

    private getMaterialsForCombinedCategories(categoryIds: string[]): MaterialDTO[] {
        return this.allMaterials.filter(material => {
            const materialCategoryIds = material.categories.map(category => category._id);
            return categoryIds.every(categoryId => materialCategoryIds.includes(categoryId));
        });
    }
}
