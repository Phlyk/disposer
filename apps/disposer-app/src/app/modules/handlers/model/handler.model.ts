import { HandlerDTO } from "@disposer/api-interfaces";
import { Distance } from "@disposer/common";

export class Handler {
    constructor(public dto: HandlerDTO, public handledMaterialIds: string[], public distanceFromUser: Distance) {}

    doesHandle(materialId: string): boolean {
        return this.handledMaterialIds.includes(materialId);
    }
}
