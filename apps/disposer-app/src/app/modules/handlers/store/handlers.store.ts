import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { AbstractStore } from "../../../core/store/abstract.store";
import { Handler } from "../model/handler.model";
import { HandlersState } from "./handlers.state";

@Injectable({
    providedIn: "root",
})
export class HandlersStore extends AbstractStore<HandlersState> {
    private readonly _prefix = "disposer";

    private readonly HANDLERS = "handlers";

    public ready$: Promise<HandlersState[keyof HandlersState][]>;

    constructor(private _storage: Storage) {
        super(new HandlersState());
        this._storage.clear();

        this.ready$ = Promise.all([
            this.initValue(this.HANDLERS),
        ]);
    }

    public setHandlers(handlers: Handler[]): Promise<Handler[]> {
        return this.setValue(this.HANDLERS, handlers) as Promise<Handler[]>;
    }

    public removeHandlers(): Promise<Handler[]> {
        return this.removeValue(this.HANDLERS) as Promise<Handler[]>;
    }

    private initValue(prop: keyof HandlersState): Promise<HandlersState[keyof HandlersState]> {
        const key = `${this._prefix}-${prop}`;
        return this._storage
            .get(key)
            .then(value => {
                this.setStateValue(prop, value);
                return value;
            })
            .catch(_ => {
                this.setStateValue(prop, undefined);
                return undefined;
            });
    }

    private setValue(prop: keyof HandlersState, value: HandlersState[keyof HandlersState]): Promise<HandlersState[keyof HandlersState]> {
        this.setStateValue(prop, value);
        const key = `${this._prefix}-${prop}`;
        return this._storage.set(key, value);
    }

    private removeValue(prop: keyof HandlersState): Promise<HandlersState[keyof HandlersState]> {
        this.setStateValue(prop, undefined);
        const key = `${this._prefix}-${prop}`;
        return this._storage.remove(key);
    }

    private setStateValue(prop: keyof HandlersState, value: HandlersState[keyof HandlersState]): void {
        this.state = {
            ...this.state,
            [prop]: value,
        };
    }
}
