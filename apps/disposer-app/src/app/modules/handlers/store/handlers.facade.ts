import { Injectable } from "@angular/core";
import { from, Observable } from "rxjs";
import { Handler } from "../model/handler.model";
import { HandlersStore } from "./Handlers.store";

@Injectable({
    providedIn: "root",
})
export class HandlersFacade {
    constructor(private store: HandlersStore) {}

    fetchHandlers(): Observable<Handler[]> {
        return from(this.store.ready$.then(_ => this.store.state.handlers));
    }

    storeHandlers(handlers: Handler[]): Observable<Handler[]> {
        return from(this.store.setHandlers(handlers));
    }
}
