import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Handler } from "../../model/handler.model";
import { HandlerService } from "../../services/handler.service";

@Component({
    selector: "disposer-handlers-page",
    templateUrl: "./handlers.page.html",
    styleUrls: ["./handlers.page.scss"],
})
export class HandlersPage implements OnInit {
    handlers$: Observable<Handler[]>;

    searchLocation = {
        name: "Current Location",
    };

    constructor(private handlerService: HandlerService) {}

    ngOnInit(): void {
        this.handlers$ = this.handlerService.fetchHandlers();
    }

    openSearchOptions() {
        console.log("opening search options");
        this.searchLocation = { name: "Fish land" };
    }
}
