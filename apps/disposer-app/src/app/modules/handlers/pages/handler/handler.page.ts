import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { Handler } from "../../model/handler.model";
import { HandlerService } from "../../services/handler.service";

@Component({
    selector: "disposer-handler-page",
    templateUrl: "./handler.page.html",
    styleUrls: ["./handler.page.scss"],
})
export class HandlerPage {
    handler$: Observable<Handler>;
    handlerImgs = [
        { url: "assets/img/austin.jpg" },
        { url: "assets/img/madison.jpg" },
        { url: "assets/img/chicago.jpg" },
        { url: "assets/img/seattle.jpg" },
    ];
    isFavourite: boolean = false;
    handlerSegment: "waste" | "details" | "about" = "waste";

    constructor(private route: ActivatedRoute, private handlerService: HandlerService) {
        this.route.paramMap.subscribe(paramMap => {
            const handlerId = paramMap.get("handlerId");
            this.handler$ = this.handlerService.getHandlerBy(handlerId);
        });
    }

    handlerSegmentChanged($event: any) {
        this.handlerSegment = $event.detail.value;
    }

    toggleFavourite() {
        this.isFavourite = !this.isFavourite;
    }
}
