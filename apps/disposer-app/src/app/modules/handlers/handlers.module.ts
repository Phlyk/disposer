import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { SlidingItemModule } from "@disposer/components";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "../../shared/shared.module";
import { HandlerPhotosComponent } from "./components/handler-photos/handler-photos.component";
import { HandlerAboutComponent } from "./components/handler/handler-about/handler-about.component";
import { HandlerDetailsComponent } from "./components/handler/handler-details/handler-details.component";
import { HandlerWasteSearchbarComponent } from "./components/handler/handler-waste/handler-waste-searchbar/handler-waste-searchbar.component";
import { HandlerWasteComponent } from "./components/handler/handler-waste/handler-waste.component";
import { HandlersComponent } from "./components/handlers/handlers.component";
import { HandlersRoutingModule } from "./handlers-routing.module";
import { HandlerPage } from "./pages/handler/handler.page";
import { HandlersPage } from "./pages/handlers/handlers.page";

@NgModule({
    declarations: [
        HandlerPage,
        HandlersPage,
        HandlersComponent,
        HandlerPhotosComponent,
        HandlerWasteComponent,
        HandlerDetailsComponent,
        HandlerAboutComponent,
        HandlerWasteSearchbarComponent,
    ],
    imports: [CommonModule, IonicModule, SharedModule, ReactiveFormsModule, HandlersRoutingModule, SlidingItemModule],
})
export class HandlersModule {}
