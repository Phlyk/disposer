import { Component, Input, OnInit } from "@angular/core";

@Component({
    selector: "disposer-handler-photos",
    templateUrl: "./handler-photos.component.html",
    styleUrls: ["./handler-photos.component.scss"],
})
export class HandlerPhotosComponent implements OnInit {
    @Input() handlerImgs: {
        url: string;
    }[];
    swiperOptions = {
        freeMode: true,
        grabCursor: true
    };
    
    constructor() {}

    ngOnInit(): void {}
}
