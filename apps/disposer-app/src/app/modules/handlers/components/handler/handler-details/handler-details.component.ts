import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { CommentSource, ContactType } from "@disposer/api-interfaces";
import { Clipboard } from "@ionic-native/clipboard/ngx";
import { Platform, ToastController } from "@ionic/angular";
import { Handler } from "../../../model/handler.model";

@Component({
    selector: "disposer-handler-details",
    templateUrl: "./handler-details.component.html",
    styleUrls: ["./handler-details.component.scss"],
})
export class HandlerDetailsComponent implements OnInit {
    @Input() handler: Handler;
    locationString: string;

    constructor(
        private router: Router,
        private clipboard: Clipboard,
        private toastController: ToastController,
        private platform: Platform
    ) {}

    ngOnInit(): void {
        this.locationString = this.makeLocationString();
    }

    showInMap() {
        this.router.navigate(["/map"]);
    }

    async copyAddress() {
        if (this.platform.is("desktop") || this.platform.is("mobile")) {
            console.warn("Copy not available on desktop or mobile versions!");
            return await this.toastController.create({ duration: 1000, message: "Copy not available on webapps" });
        }
        await this.clipboard.copy(this.locationString);
        await this.toastController.create({ duration: 1000, message: "Copied..." });
    }

    async copyPhone(contactNumber: string) {
        if (this.platform.is("desktop") || this.platform.is("mobile")) {
            console.warn("Copy not available on desktop or mobile versions!");
            return await this.toastController.create({ duration: 1000, message: "Copy not available on webapps" });
        }
        await this.toastController.create({ duration: 1000, message: "Copied..." });
        await this.clipboard.copy(contactNumber);
    }

    addAWebsite() {
        this.toastController.create({ message: "Sorry, you can't do that yet ;)", duration: 1000 });
    }

    contactTypeToIcon(contactType: ContactType): string {
        switch (contactType) {
            case ContactType.EMAIL:
                return "mail-outline";
            case ContactType.WA:
                return "logo-whatsapp";
            case ContactType.PHONE:
                return "call-outline";
            default:
                return "call-outline";
        }
    }

    commentSourceToName(commentSource: CommentSource): string {
        switch (commentSource) {
            case CommentSource.HANDLER:
                return CommentSource.HANDLER;
            case CommentSource.USER:
                return CommentSource.USER;
            default:
                return CommentSource.HANDLER;
        }
    }

    private makeLocationString() {
        return `${this.handler.dto.location.address}, ${this.handler.dto.location.city}, ${this.handler.dto.location.postcode}`;
    }
}
