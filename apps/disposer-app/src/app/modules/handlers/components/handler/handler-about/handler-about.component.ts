import { Component, Input, OnInit } from "@angular/core";
import { Handler } from "../../../model/handler.model";

@Component({
    selector: "disposer-handler-about",
    templateUrl: "./handler-about.component.html",
    styleUrls: ["./handler-about.component.scss"],
})
export class HandlerAboutComponent implements OnInit {
    @Input() handler: Handler;

    ngOnInit(): void {}
}
