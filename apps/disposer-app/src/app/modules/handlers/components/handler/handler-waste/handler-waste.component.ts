import { Component, Input, OnInit } from "@angular/core";
import { MaterialDTO } from "@disposer/api-interfaces";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { MaterialsService } from "../../../../materials/services/materials.service";

@Component({
    selector: "disposer-handler-waste",
    templateUrl: "./handler-waste.component.html",
    styleUrls: ["./handler-waste.component.scss"],
})
export class HandlerWasteComponent implements OnInit {
    @Input() handledWasteIds: string[];
    handledWastes$: Observable<MaterialDTO[]>;

    searching: boolean;

    constructor(private materialService: MaterialsService) {}

    ngOnInit(): void {
        this.applySearch("");
    }

    openSearch() {
        this.searching = true;
    }

    applySearch(searchTerm: string) {
        searchTerm = searchTerm.toLowerCase();
        this.handledWastes$ = this.materialService.getMaterialsBy(this.handledWasteIds).pipe(
            map(handledMaterials =>
                handledMaterials.filter(material => {
                    return (
                        material.name.toLowerCase().includes(searchTerm) ||
                        material.description?.toLowerCase().includes(searchTerm)
                    );
                })
            )
        );
    }

    searchCanceled() {
        this.searching = false;
        this.applySearch("");
    }
}
