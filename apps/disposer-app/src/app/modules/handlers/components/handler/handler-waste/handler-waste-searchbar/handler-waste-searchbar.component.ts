import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from "@angular/core";
import { FormControl } from "@angular/forms";
import { IonSearchbar } from "@ionic/angular";
import { Subject } from "rxjs";
import { debounceTime, distinctUntilChanged, takeUntil } from "rxjs/operators";

@Component({
    selector: "disposer-handler-waste-searchbar",
    templateUrl: "./handler-waste-searchbar.component.html",
    styleUrls: ["./handler-waste-searchbar.component.scss"],
})
export class HandlerWasteSearchbarComponent implements OnInit, OnDestroy {
    @Input() searchName: string;
    @Input() searchPlaceholder: string;
    @Output() searching = new EventEmitter<string>();
    @Output() searchCanceled = new EventEmitter<string>();
    @ViewChild(IonSearchbar) ionSearchbar: IonSearchbar;

    defaultSearchPlaceholder = "Search all ";
    readonly noResultsMessage = "No results found. Try being less specific";
    
    searchControl: FormControl;
    searchTerm = "";
    isSearching: boolean;

    private destroyed$ = new Subject();

    constructor() {
        this.searchControl = new FormControl();
    }

    ngOnInit(): void {
        this.defaultSearchPlaceholder += this.searchName;
        if (!this.searchPlaceholder) {
            this.searchPlaceholder = this.defaultSearchPlaceholder;
        }

        this.searchControl.valueChanges.pipe(
            debounceTime(400),
            distinctUntilChanged(),
            takeUntil(this.destroyed$)
        ).subscribe(searchTerm => {
            this.isSearching = false;
            this.searchTerm = searchTerm;
            this.applySearch();
        });
    }

    applySearch() {
        this.searching.next(this.searchTerm);
    }

    ngAfterViewInit(): void {
        setTimeout(async () => {
            const inputElement = await this.ionSearchbar.getInputElement();
            inputElement.focus();
        }, 500);
    }

    onSearchInput() {
        this.isSearching = true;
    }

    resetSearchTerm() {
        this.searchTerm = "";
    }

    onSearchCanceled() {
        this.searchCanceled.emit();
    }

    ngOnDestroy() {
        this.destroyed$.next();
        this.destroyed$.complete();
    }
}
