import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Handler } from "../../model/handler.model";

@Component({
    selector: "disposer-handlers",
    templateUrl: "./handlers.component.html",
    styleUrls: ["./handlers.component.scss"],
})
export class HandlersComponent implements OnInit {
    @Input() handlers: Handler[];

    constructor(private router: Router) {}

    ngOnInit(): void {}

    segmentChanged($event: any) {
        if ($event.detail["value"] == "map") {
            console.log("changing to map now");
            this.router.navigate(["map"]);
        }
    }
}
