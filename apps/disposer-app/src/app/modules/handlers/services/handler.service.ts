import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HandlerDTO } from "@disposer/api-interfaces";
import { environment } from "apps/disposer-app/src/environments/environment";
import { iif, Observable, of } from "rxjs";
import { concatMap, filter, map, switchMap, take } from "rxjs/operators";
import { HandlerFactory } from "../model/handler.factory";
import { Handler } from "../model/handler.model";
import { HandlersFacade } from "../store/handlers.facade";

@Injectable({
    providedIn: "root",
})
export class HandlerService {
    constructor(private httpClient: HttpClient, private handlersFacade: HandlersFacade, private handlerFactory: HandlerFactory) {}

    fetchHandlers(): Observable<Handler[]> {
        const fetchAndStoreHandlers$ = this.httpClient.get<HandlerDTO[]>(`${environment.apiEndpoint}/handlers`).pipe(
            map(handlers => handlers.map(handlerDto => this.handlerFactory.createHandlerFrom(handlerDto))),
            map(handlers =>
                handlers.sort((a, b) => a.distanceFromUser.distance.valueOf() - b.distanceFromUser.distance.valueOf())
            ),
            switchMap(handlers => this.handlersFacade.storeHandlers(handlers))
        );
        const isFactoryReady$: Observable<boolean> = this.handlerFactory.ready$.pipe(
            filter(value => value),
            take(1)
        );
        return isFactoryReady$.pipe(
            concatMap(_ =>
                this.handlersFacade
                    .fetchHandlers()
                    .pipe(
                        switchMap(storedHandlers =>
                            iif(() => !!(storedHandlers && storedHandlers.length), of(storedHandlers), fetchAndStoreHandlers$)
                        )
                    )
            )
        );
    }

    getHandlerBy(handlerId: string): Observable<Handler> {
        return this.fetchHandlers().pipe(map(handlers => handlers.find(handler => handler.dto._id == handlerId)));
    }

    getHandlersFor(materialId: string): Observable<Handler[]> {
        return this.fetchHandlers().pipe(map(handlers => handlers.filter(handler => handler.doesHandle(materialId))));
    }
}
