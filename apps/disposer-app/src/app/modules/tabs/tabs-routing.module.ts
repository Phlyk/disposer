import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./page/tabs.page";

const routes: Routes = [
    {
        path: "",
        component: TabsPage,
        children: [
            {
                path: "materials",
                loadChildren: () => import("../../modules/materials/materials.module").then(m => m.MaterialsModule),
            },
            {
                path: "handlers",
                loadChildren: () => import("../../modules/handlers/handlers.module").then(m => m.HandlersModule),
            },
            {
                path: "learn",
                loadChildren: () => import("../../modules/learning/learning.module").then(m => m.LearningModule),
            },
            {
                path: "map",
                loadChildren: () => import("../../modules/map/map.module").then(m => m.MapModule),
            },
            {
                path: "about",
                loadChildren: () => import("../../modules/about/about.module").then(m => m.AboutModule),
            },
            {
                path: "",
                redirectTo: "/materials",
                pathMatch: "full",
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class TabsRoutingModule {}
