import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from "@ionic/angular";
import { TabsPage } from "./page/tabs.page";
import { TabsRoutingModule } from './tabs-routing.module';



@NgModule({
    declarations: [TabsPage],
    imports: [
        CommonModule,
        IonicModule,
        TabsRoutingModule
    ],
    exports: [TabsPage]
})
export class TabsModule { }
