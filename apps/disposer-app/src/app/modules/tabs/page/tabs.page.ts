import { Component } from "@angular/core";

@Component({
    selector: "disposer-tabs",
    templateUrl: "tabs.page.html",
    styleUrls: ["tabs.page.scss"],
})
export class TabsPage {}
