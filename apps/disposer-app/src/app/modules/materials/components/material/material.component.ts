import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { MaterialDTO } from "@disposer/api-interfaces";

@Component({
    selector: "disposer-material",
    templateUrl: "./material.component.html",
    styleUrls: ["./material.component.scss"]
})
export class MaterialComponent {
    @Input() material: MaterialDTO;

    constructor(private router: Router) {}

    navigateToMaterial() {
        this.router.navigateByUrl(`/materials/${this.material._id}`, {state: this.material});
    }
}
