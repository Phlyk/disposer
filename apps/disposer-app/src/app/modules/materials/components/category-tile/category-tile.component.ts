import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Router } from "@angular/router";
import { CategoryDTO } from "@disposer/api-interfaces";

@Component({
    selector: "disposer-category-tile",
    templateUrl: "./category-tile.component.html",
    styleUrls: ["./category-tile.component.scss"],
})
export class CategoryTileComponent {
    @Input() category: CategoryDTO;
    @Input() compositeSearchActive: boolean;
    @Output() tileSelected = new EventEmitter<string>();

    readonly LONG_PRESS_DELAY = 500;

    _tileSelected = false;

    constructor(private router: Router) {}

    tileClicked(category: CategoryDTO) {
        if (this._tileSelected) {
            this.resetSelection();
            return this.tileSelected.emit(this.category._id);
        }
        if (this.compositeSearchActive) {
            this._tileSelected = true;
            return this.tileSelected.emit(this.category._id);
        }
        this.router.navigateByUrl(`materials/category/${category._id}`);
    }

    tileLongPressed() {
        this._tileSelected = true;
        this.tileSelected.emit(this.category._id);
    }

    resetSelection() {
        this._tileSelected = false;
    }
}
