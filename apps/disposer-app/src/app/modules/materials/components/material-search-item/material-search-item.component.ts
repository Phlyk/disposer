import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { MaterialDTO } from "@disposer/api-interfaces";

@Component({
    selector: "disposer-material-search-item",
    templateUrl: "./material-search-item.component.html",
    styleUrls: ["./material-search-item.component.scss"],
})
export class MaterialSearchItemComponent {
    @Input() material: MaterialDTO;
    
    constructor(private router: Router) {}

    navigateTo(material: MaterialDTO) {
        this.router.navigate([`materials/${material._id}`]);
    }
}
