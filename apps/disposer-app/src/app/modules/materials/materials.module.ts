import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { SlidingItemModule } from "@disposer/components";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "../../shared/shared.module";
import { CategoryTileComponent } from './components/category-tile/category-tile.component';
import { MaterialSearchItemComponent } from './components/material-search-item/material-search-item.component';
import { MaterialComponent } from "./components/material/material.component";
import { MaterialsRoutingModule } from "./materials-routing.module";
import { CategoriesPage } from "./pages/categories/categories.page";
import { MaterialSearchPage } from "./pages/material-search/material-search.page";
import { MaterialPage } from "./pages/material/material.page";
import { MaterialsPage } from "./pages/materials/materials.page";

@NgModule({
    declarations: [
        CategoriesPage,
        CategoryTileComponent,

        MaterialPage,
        MaterialsPage,
        MaterialSearchPage,
        
        MaterialComponent,
        
        MaterialSearchItemComponent,
    ],
    imports: [
        CommonModule,

        ReactiveFormsModule,
        IonicModule,

        SharedModule,
        SlidingItemModule,

        MaterialsRoutingModule,
    ],
})
export class MaterialsModule {}
