import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CategoryDTO, MaterialDTO } from "@disposer/api-interfaces";
import { BehaviorSubject, iif, Observable, of } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { environment } from "../../../../environments/environment";
import { MaterialsFacade } from "../store/materials.facade";

@Injectable({
    providedIn: "root",
})
export class MaterialsService {
    private searchCategories$ = new BehaviorSubject<CategoryDTO[]>([])

    constructor(private httpClient: HttpClient, private materialsFacade: MaterialsFacade) {}

    fetchMaterials(): Observable<MaterialDTO[]> {
        const fetchAndStoreMaterials$ = this.httpClient
            .get<MaterialDTO[]>(`${environment.apiEndpoint}/materials`)
            .pipe(switchMap(materials => this.materialsFacade.storeMaterials(materials)));
        return this.materialsFacade
            .fetchMaterials()
            .pipe(
                switchMap(storedMaterials =>
                    iif(() => !!(storedMaterials && storedMaterials.length), of(storedMaterials), fetchAndStoreMaterials$)
                )
            );
    }

    fetchCategories(): Observable<CategoryDTO[]> {
        const fetchAndStoreCategories$ = this.httpClient
            .get<CategoryDTO[]>(`${environment.apiEndpoint}/materials/categories`)
            .pipe(switchMap(categories => this.materialsFacade.storeCategories(categories)));
        return this.materialsFacade
            .fetchCategories()
            .pipe(
                switchMap(storedCategories =>
                    iif(() => !!(storedCategories && storedCategories.length), of(storedCategories), fetchAndStoreCategories$)
                )
            );
    }

    getCategoryBy(categoryId: string): Observable<CategoryDTO> {
        return this.fetchCategories().pipe(map(categories => categories.find(category => category._id === categoryId)));
    }

    getMaterialBy(materialId: string): Observable<MaterialDTO> {
        return this.fetchMaterials().pipe(map(materials => materials.find(material => material._id === materialId)));
    }

    getMaterialsBy(materialIds: string[]): Observable<MaterialDTO[]> {
        return this.fetchMaterials().pipe(map(materials => materials.filter(material => materialIds.includes(material._id))));
    }

    filterMaterialsBy(categoryId: string): Observable<MaterialDTO[]> {
        return this.fetchMaterials().pipe(
            map(materials => {
                if (!categoryId) return materials;
                return materials.filter(material => material.categories.find(category => category._id == categoryId));
            })
        );
    }

    searchMaterialsBy(searchTerm: string): Observable<MaterialDTO[]> {
        searchTerm = searchTerm.toLowerCase();
        return this.fetchMaterials().pipe(
            map(materials => {
                const materialsByCategory = this.filterMaterialsByCategories(materials);
                return this._filterMaterialsBy(materialsByCategory, searchTerm);
            })
        );
    }

    getSearchCategories(): Observable<CategoryDTO[]> {
        return this.searchCategories$.asObservable();
    }

    setSearchCategories(compositeSearchCategories: CategoryDTO[]) {
        this.searchCategories$.next(compositeSearchCategories);
    }

    resetSearchCategories() {
        this.searchCategories$.next([]);
    }

    removeSearchCategory(categoryIndex: number) {
        this.searchCategories$.value.splice(categoryIndex, 1);
        this.searchCategories$.next(this.searchCategories$.value);
    }

    private filterMaterialsByCategories(materials: MaterialDTO[]): MaterialDTO[] {
        if (!this.searchCategories$.value.length) {
            return materials;
        }

        return materials.filter(material =>
            this.searchCategories$.value.every(cat => !!material.categories.find(matCat => matCat._id === cat._id))
        );
    }

    private _filterMaterialsBy(materials: MaterialDTO[], searchTerm: string): MaterialDTO[] {
        return materials.filter(material => {
            return material.name.toLowerCase().includes(searchTerm) || material.description?.toLowerCase().includes(searchTerm);
        });
    }
}
