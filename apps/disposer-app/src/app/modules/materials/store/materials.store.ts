import { Injectable } from "@angular/core";
import { CategoryDTO, MaterialDTO } from "@disposer/api-interfaces";
import { Storage } from "@ionic/storage";
import { AbstractStore } from "../../../core/store/abstract.store";
import { MaterialsState } from "./materials.state";

@Injectable({
    providedIn: "root",
})
export class MaterialsStore extends AbstractStore<MaterialsState> {
    private readonly _prefix = "disposer";

    private readonly MATERIALS = "materials";
    private readonly CATEGORIES = "categories";

    public ready$: Promise<MaterialsState[keyof MaterialsState][]>;

    constructor(private _storage: Storage) {
        super(new MaterialsState());
        this._storage.clear();

        this.ready$ = Promise.all([
            this.initValue(this.MATERIALS),
            this.initValue(this.CATEGORIES),
        ]);
    }

    public setMaterials(materials: MaterialDTO[]): Promise<MaterialDTO[]> {
        return this.setValue(this.MATERIALS, materials) as Promise<MaterialDTO[]>;
    }

    public removeMaterials(): Promise<MaterialDTO[]> {
        return this.removeValue(this.MATERIALS) as Promise<MaterialDTO[]>;
    }

    public setCategories(categories: CategoryDTO[]): Promise<CategoryDTO[]> {
        return this.setValue(this.CATEGORIES, categories);
    }

    public removeCategories(): Promise<CategoryDTO[]> {
        return this.removeValue(this.CATEGORIES);
    }

    private initValue(prop: keyof MaterialsState): Promise<MaterialsState[keyof MaterialsState]> {
        const key = `${this._prefix}-${prop}`;
        return this._storage
            .get(key)
            .then(value => {
                this.setStateValue(prop, value);
                return value;
            })
            .catch(_ => {
                this.setStateValue(prop, undefined);
                return undefined;
            });
    }

    private setValue(prop: keyof MaterialsState, value: MaterialsState[keyof MaterialsState]): Promise<MaterialsState[keyof MaterialsState]> {
        this.setStateValue(prop, value);
        const key = `${this._prefix}-${prop}`;
        return this._storage.set(key, value);
    }

    private removeValue(prop: keyof MaterialsState): Promise<MaterialsState[keyof MaterialsState]> {
        this.setStateValue(prop, undefined);
        const key = `${this._prefix}-${prop}`;
        return this._storage.remove(key);
    }

    private setStateValue(prop: keyof MaterialsState, value: MaterialsState[keyof MaterialsState]): void {
        this.state = {
            ...this.state,
            [prop]: value,
        };
    }
}
