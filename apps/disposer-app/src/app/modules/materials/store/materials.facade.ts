import { Injectable } from "@angular/core";
import { CategoryDTO, MaterialDTO } from "@disposer/api-interfaces";
import { from, Observable } from "rxjs";
import { MaterialsStore } from "./materials.store";

@Injectable({
    providedIn: "root",
})
export class MaterialsFacade {
    constructor(private store: MaterialsStore) {}

    fetchMaterials(): Observable<MaterialDTO[]> {
        return from(this.store.ready$.then(_ => this.store.state.materials));
    }

    storeMaterials(materials: MaterialDTO[]): Observable<MaterialDTO[]> {
        return from(this.store.setMaterials(materials));
    }

    fetchCategories(): Observable<CategoryDTO[]> {
        return from(this.store.ready$.then(_ => this.store.state.categories));
    }

    getCategoryBy(categoryId: string): CategoryDTO {
        return this.store.state.categories.find(category => category._id == categoryId);
    }

    storeCategories(categories: CategoryDTO[]): Observable<CategoryDTO[]> {
        return from(this.store.setCategories(categories));
    }
}
