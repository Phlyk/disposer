import { CategoryDTO, MaterialDTO } from "@disposer/api-interfaces";

export class MaterialsState {
    public readonly materials: MaterialDTO[];
    public readonly categories: CategoryDTO[];
}

