import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CategoriesPage } from "./pages/categories/categories.page";
import { MaterialSearchPage } from "./pages/material-search/material-search.page";
import { MaterialPage } from "./pages/material/material.page";
import { MaterialsPage } from "./pages/materials/materials.page";

const routes: Routes = [
    {
        path: "",
        component: CategoriesPage,
    },
    {
        path: "search",
        component: MaterialSearchPage,
    },
    {
        path: "category/:categoryId",
        component: MaterialsPage,
    },
    {
        path: ":materialId",
        component: MaterialPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MaterialsRoutingModule {}
