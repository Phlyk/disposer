import { Component, OnInit, QueryList, ViewChildren } from "@angular/core";
import { Router } from "@angular/router";
import { CategoryDTO } from "@disposer/api-interfaces";
import { Observable } from "rxjs";
import { CategoryTileComponent } from "../../components/category-tile/category-tile.component";
import { MaterialsService } from "../../services/materials.service";

@Component({
    selector: "disposer-material-categories-page",
    templateUrl: "./categories.page.html",
    styleUrls: ["./categories.page.scss"],
})
export class CategoriesPage implements OnInit {
    @ViewChildren(CategoryTileComponent) categoryTiles: QueryList<CategoryTileComponent>;
    categories$: Observable<CategoryDTO[]>;

    compositeSearchCategories: CategoryDTO[] = [];
    compositeSearchActive = false;

    constructor(private router: Router, private materialsService: MaterialsService) {}

    ngOnInit(): void {
        this.categories$ = this.materialsService.fetchCategories();
    }

    navigateToSearch() {
        this.materialsService.setSearchCategories(Array.from(this.compositeSearchCategories));
        this.router.navigate(["materials/search"]);
    }

    compositeCategorySearch(category: CategoryDTO) {
        this.compositeSearchActive = true;
        const categoryIndex = this.compositeSearchCategories.findIndex(arrayCat => category == arrayCat);
        if (categoryIndex !== -1) {
            this.compositeSearchCategories.splice(categoryIndex, 1);
            if (!this.compositeSearchCategories.length) this.compositeSearchActive = false;
        } else {
            this.compositeSearchCategories.push(category);
        }
    }

    clearCompositeSearch() {
        this.compositeSearchCategories = [];
        this.categoryTiles.forEach(tile => tile.resetSelection());
        this.compositeSearchActive = false;
        this.materialsService.resetSearchCategories();
    }
}
