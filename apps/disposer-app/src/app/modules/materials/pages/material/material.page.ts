import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MaterialDTO } from "@disposer/api-interfaces";
import { Observable } from "rxjs";
import { Handler } from "../../../handlers/model/handler.model";
import { HandlerService } from "../../../handlers/services/handler.service";
import { MaterialsService } from "../../services/materials.service";

@Component({
    selector: "disposer-material-page",
    templateUrl: "./material.page.html",
    styleUrls: ["./material.page.scss"],
})
export class MaterialPage {
    material$: Observable<MaterialDTO>;
    handlersForMat$: Observable<Handler[]>;

    searchLocation = {
        name: "Current Location",
    };

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private materialsService: MaterialsService,
        private handlersService: HandlerService
    ) {
        const materialId: string = this.route.snapshot.paramMap.get("materialId");
        this.material$ = this.materialsService.getMaterialBy(materialId);

        this.handlersForMat$ = this.handlersService.getHandlersFor(materialId);
    }

    segmentChanged($event: any) {
        if ($event.detail["value"] == "map") {
            console.log("changing to map now");
            this.router.navigate(["map"]);
        }
    }

    openSearchOptions() {
        console.log("opening search options");
        this.searchLocation = { name: "Fish land" };
    }
}
