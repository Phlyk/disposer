import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormControl } from "@angular/forms";
import { CategoryDTO, MaterialDTO } from "@disposer/api-interfaces";
import { IonInfiniteScroll, IonSearchbar } from "@ionic/angular";
import { Observable, Subscription } from "rxjs";
import { debounceTime, map } from "rxjs/operators";
import { MaterialsService } from "../../services/materials.service";

@Component({
    selector: "disposer-material-search-page",
    templateUrl: "./material-search.page.html",
    styleUrls: ["./material-search.page.scss"],
})
export class MaterialSearchPage implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
    @ViewChild(IonSearchbar) ionSearchbar: IonSearchbar;

    private NB_SEARCH_ITEMS = 20;

    materials$: Observable<MaterialDTO[]>;

    readonly defaultSearchPlaceholder = "Search all materials";
    readonly noResultsMessage = "No results found. Try being less specific";
    searchPlaceholder: string;

    searchCategories: CategoryDTO[] = [];
    searchCategorySubscription: Subscription;
    searchTerm = "";
    searchControl: FormControl;
    searching: boolean;

    constructor(private materialsService: MaterialsService) {
        this.searchControl = new FormControl();
    }

    ngOnInit(): void {
        this.searchCategorySubscription = this.materialsService.getSearchCategories().subscribe(searchCategories => {
            this.searchCategories = searchCategories;
            this.searchPlaceholder = this.makeSearchPlaceholder();
        });
        this.searchPlaceholder = this.makeSearchPlaceholder();

        this.applySearch();

        this.searchControl.valueChanges.pipe(debounceTime(400)).subscribe(searchTerm => {
            this.searching = false;
            this.searchTerm = searchTerm;
            this.applySearch();
        });
    }

    ngAfterViewInit(): void {
        setTimeout(async () => {
            const inputElement = await this.ionSearchbar.getInputElement();
            inputElement.focus();
        }, 500);
    }

    onSearchInput() {
        this.searching = true;
    }

    resetSearchCategoriesAndTerm() {
        this.searching = !!this.searchTerm || !!this.searchCategories.length;
        this.materialsService.resetSearchCategories();
        this.searchPlaceholder = this.defaultSearchPlaceholder;
        this.resetSearchTerm();
    }

    resetSearchTerm() {
        this.NB_SEARCH_ITEMS = 20;
        this.searchControl.setValue("");
    }

    searchMoreMaterials() {
        this.NB_SEARCH_ITEMS += 20;
        setTimeout(() => {
            this.infiniteScroll.complete();
            this.applySearch();

            if (this.NB_SEARCH_ITEMS >= 220) {
                // TODO - disable the infinite scroll when finished... by keeping track of items received
                this.infiniteScroll.disabled = true;
            }
        }, 500);
    }

    removeSearchCategory(categoryIndex: number) {
        this.materialsService.removeSearchCategory(categoryIndex);
        this.applySearch();
    }

    trackBy(_: number, item: MaterialDTO) {
        return item._id;
    }

    private applySearch() {
        this.materials$ = this.materialsService
            .searchMaterialsBy(this.searchTerm)
            .pipe(map(filteredMaterials => filteredMaterials.slice(0, this.NB_SEARCH_ITEMS)));
    }

    private makeSearchPlaceholder(): string {
        if (!this.searchCategories.length) {
            return this.defaultSearchPlaceholder;
        }
        return this.searchCategories.reduce((stringAcc, cat) => (stringAcc += `${cat.name} `), "Searching in: ");
    }

    ngOnDestroy() {
        this.searchCategorySubscription.unsubscribe();
    }
}
