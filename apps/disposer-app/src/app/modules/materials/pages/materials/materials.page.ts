import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CategoryDTO, MaterialDTO } from "@disposer/api-interfaces";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { MaterialsService } from "../../services/materials.service";

@Component({
    selector: "disposer-materials-page",
    templateUrl: "./materials.page.html",
    styleUrls: ["./materials.page.scss"],
})
export class MaterialsPage {
    materials$: Observable<MaterialDTO[]>;
    materialsByLetter$: Observable<Map<string, MaterialDTO[]>>;
    category$: Observable<CategoryDTO>;

    constructor(private materialsService: MaterialsService, private route: ActivatedRoute) {
        this.route.paramMap.subscribe(paramMap => {
            const categoryId = paramMap.get("categoryId");
            this.initialiseMaterials(categoryId);
        });
    }

    private initialiseMaterials(categoryId: string) {
        this.materials$ = this.materialsService.filterMaterialsBy(categoryId);
        this.materialsByLetter$ = this.materialsService.filterMaterialsBy(categoryId).pipe(map(materials => {
            const groupedMap = new Map<string, MaterialDTO[]>();
            materials.forEach(material => {
                const groupName = material.name.charAt(0).toUpperCase()
                const group = groupedMap.get(groupName);
                if (!group) {
                    groupedMap.set(groupName, [material])
                } else {
                    group.push(material)
                }
            })
            return groupedMap;
        }))
        this.category$ = this.materialsService.getCategoryBy(categoryId);
    }
    
}
