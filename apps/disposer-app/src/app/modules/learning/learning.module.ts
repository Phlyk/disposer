import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { LearningComponent } from "./components/learning.component";
import { LearningRoutingModule } from "./learning-routing.module";
import { LearningPage } from "./page/Learning.page";

@NgModule({
    declarations: [LearningPage, LearningComponent],
    imports: [CommonModule, IonicModule, LearningRoutingModule],
})
export class LearningModule {}
