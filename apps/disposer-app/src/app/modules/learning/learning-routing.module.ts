import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LearningPage } from "./page/Learning.page";

const routes: Routes = [
    {
        path: "",
        component: LearningPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LearningRoutingModule {}
