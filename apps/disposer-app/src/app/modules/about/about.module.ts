import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { AboutPage } from "./pages/about/about.page";

@NgModule({
    declarations: [AboutPage],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: "",
                component: AboutPage,
            },
        ]),
    ],
})
export class AboutModule {}
