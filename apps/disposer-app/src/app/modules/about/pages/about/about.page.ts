import { DOCUMENT } from "@angular/common";
import { Component, Inject, OnInit } from "@angular/core";

@Component({
    selector: "disposer-about",
    templateUrl: "./about.page.html",
    styleUrls: ["./about.page.scss"],
})
export class AboutPage implements OnInit {
    dark = false;

    constructor(
        @Inject(DOCUMENT) private doc: Document,
    ) {}

    ngOnInit(): void {}

    toggleDarkTheme() {
        const appEl = this.doc.querySelector("ion-app");
        if (appEl.classList.contains("dark-theme")) {
            appEl.classList.remove("dark-theme");
        } else {
            appEl.classList.add("dark-theme");
        }
    }
}
