import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
import { Clipboard } from "@ionic-native/clipboard/ngx";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { IonicStorageModule } from "@ionic/storage";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { TabsModule } from "./modules/tabs/tabs.module";

@NgModule({
    declarations: [AppComponent],
    imports: [BrowserModule, IonicModule.forRoot(), IonicStorageModule.forRoot(), HttpClientModule, AppRoutingModule, TabsModule],
    providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, Geolocation, Clipboard],
    bootstrap: [AppComponent],
})
export class AppModule {}
