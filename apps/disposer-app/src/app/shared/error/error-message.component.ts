import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'disposer-error',
    template: `
        <div class="error">
            <div>Phrashnams!</div>
            <code>{{error.message}}</code>
        </div>
    `,
})
export class ErrorMessageComponent implements OnInit {
    @Input() error: Error;
    
    ngOnInit(): void {
        console.error("Error: ", this.error);
    }
}
