import { NgModule } from '@angular/core';
import { WithLoadingSimplePipe } from './with-loading-simple.pipe';
import { WithLoadingPipe } from './with-loading.pipe';

@NgModule({
    declarations: [
        WithLoadingPipe,
        WithLoadingSimplePipe,
    ],
    exports: [
        WithLoadingPipe,
        WithLoadingSimplePipe,
    ],
})
export class PipeModule { }
