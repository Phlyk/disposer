import { Directive, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, Renderer2 } from "@angular/core";
import { Subscription, timer } from "rxjs";

@Directive({
    selector: "[appLongPress]",
})
export class LongPressDirective implements OnInit {
    timerSub: Subscription;

    @Input() delay = 250;
    @Output() longPressed = new EventEmitter<any>();
    @Output() shortClick = new EventEmitter<any>();

    constructor(private elementRef: ElementRef<HTMLElement>, private renderer: Renderer2) {}

    ngOnInit() {
        const isTouch = "ontouchstart" in document.documentElement;
        if (isTouch) {
            this.renderer.listen(this.elementRef.nativeElement, "pointerleave", () => this.unsub());
        }
    }

    @HostListener("pointerdown", ["$event"])
    onPointerDown(event: PointerEvent) {
        // don't do right/middle clicks
        if (event.button !== 0) return;

        this.timerSub = timer(this.delay).subscribe(() => {
            this.longPressed.emit(event);
        });
    }

    @HostListener("pointerup", ["$event"])
    onPointerUp(event: PointerEvent) {
        if (this.timerSub && !this.timerSub.closed) {
            this.timerSub.unsubscribe();
            event.stopPropagation();
            this.shortClick.emit(event);
        }
    }

    @HostListener("pointercancel")
    onPointerCancel() {
        this.unsub();
    }

    onPointerLeave() {
        this.unsub();
    }

    private unsub() {
        if (this.timerSub && !this.timerSub.closed) {
            this.timerSub.unsubscribe();
        }
    }
}
