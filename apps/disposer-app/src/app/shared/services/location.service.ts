import { Injectable } from "@angular/core";
import { DCoordinates, Distance } from "@disposer/common";
import { Geolocation, Geoposition } from "@ionic-native/geolocation/ngx";
import { ToastController } from "@ionic/angular";

@Injectable({ providedIn: "root" })
export class LocationService {
    geoposition: Geoposition;

    constructor(private geolocation: Geolocation, private toastController: ToastController) {}

    async getLocation(): Promise<Geoposition> {
        if (this.geoposition) {
            return Promise.resolve(this.geoposition);
        }
        return this.geolocation
            .getCurrentPosition()
            .then((geoposition: Geoposition) => {
                this.geoposition = geoposition;
                return geoposition;
            })
            .catch(async error => {
                console.error(error);
                this.grillSomeBread();
                this.geoposition = {
                    coords: {
                        latitude: 11.77,
                        longitude: 75.88,
                        accuracy: 1,
                        altitude: 1,
                        heading: 1,
                        speed: 1,
                        altitudeAccuracy: 1,
                    },
                    timestamp: 1,
                };
                return this.geoposition;
            });
    }

    getDistanceBetween(pointA: DCoordinates, pointB: DCoordinates, unit: "miles" | "km" = "km"): Distance {
        const EARTH_RADIUS = unit === "km" ? 6371.071 : 3958.8;

        const convertToRadians = position => position * (Math.PI / 180);

        const pointARadianLat = convertToRadians(pointA.lat);
        const pointBRadianLat = convertToRadians(pointB.lat);
        const pointLatDifference = pointARadianLat - pointARadianLat;
        const pointLongDifference = convertToRadians(pointB.long - pointA.long);

        const distance: Number =
            2 *
            EARTH_RADIUS *
            Math.asin(
                Math.sqrt(
                    Math.sin(pointLatDifference / 2) * Math.sin(pointLatDifference / 2) +
                        Math.cos(pointARadianLat) *
                            Math.cos(pointBRadianLat) *
                            Math.sin(pointLongDifference / 2) *
                            Math.sin(pointLongDifference / 2)
                )
            );
        return { distance: Number(distance.toFixed(2)), unit };
    }

    private async grillSomeBread(): Promise<void> {
        const toast = await this.toastController.create({
            message: "Geolocation could not be determined, using default...",
            duration: 5000,
        });
        toast.present();
    }
}
