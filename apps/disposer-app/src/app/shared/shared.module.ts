import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from "@ionic/angular";
import { LongPressDirective } from "./directives/long-press.directive";
import { ErrorMessageComponent } from "./error/error-message.component";
import { WasteIcon } from './icons/waste/waste.icon';
import { PipeModule } from "./pipes/pipe.module";

@NgModule({
    declarations: [
        ErrorMessageComponent,
        WasteIcon,

        LongPressDirective
    ],
    imports: [
        CommonModule,
        PipeModule,
        IonicModule
    ],
    exports: [
        ErrorMessageComponent,
        WasteIcon,

        LongPressDirective,

        PipeModule
    ],
})
export class SharedModule { }
