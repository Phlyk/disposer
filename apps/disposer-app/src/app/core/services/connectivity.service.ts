import { Injectable } from "@angular/core";
import { Network } from "@ionic-native/network/ngx";
import { Platform } from "@ionic/angular";

@Injectable()
export class ConnectivityService {
    onDevice: boolean;
    private _isOnline: boolean;

    constructor(public platform: Platform, private network: Network) {
        console.log(this.platform);

        this.network.onChange().subscribe(flerps => this._isOnline = !!flerps);
    }

    isOnline(): boolean {
        if (this.platform.is("desktop")) {
            return this._isOnline;
        }
        return navigator.onLine;
    }

    isOffline(): boolean {
        if (this.platform.is("desktop")) {
            return !this._isOnline;
        }
        return navigator.onLine;
    }
}
