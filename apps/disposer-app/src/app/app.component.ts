import { Component } from "@angular/core";

@Component({
    selector: "disposer-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"],
})
export class AppComponent {
}
