import { LocationType } from "@disposer/api-interfaces";
import { Prop, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export class DLocation extends Document {
    @Prop({ required: true })
    address: string;

    @Prop({ required: true })
    city: string;

    @Prop()
    state: string;

    @Prop({ default: "India" })
    country: string;

    @Prop({ required: true })
    postcode: string;

    @Prop()
    landmark?: string;

    @Prop({ type: Number, enum: Object.values(LocationType), default: LocationType.HANDLER })
    locationType?: LocationType;

    @Prop({ required: true })
    latitude: number;

    @Prop({ required: true })
    longitude: number;

    @Prop([String])
    photos?: string[];
}

export const LocationSchema = SchemaFactory.createForClass(DLocation);
