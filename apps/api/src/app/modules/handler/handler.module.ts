import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { HandlerController } from "./handler.controller";
import { HandlerService } from "./handler.service";
import { Handler, HandlerSchema } from "./schema/handler.schema";


@Module({
    imports: [MongooseModule.forFeature([
        { name: Handler.name, schema: HandlerSchema }
    ])],
    controllers: [HandlerController],
    providers: [HandlerService]
})
export class HandlerModule { }
