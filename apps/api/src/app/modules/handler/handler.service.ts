import { Injectable } from "@angular/core";
import { BulkCreateHandlerResponseDTO, HandlerDTO } from "@disposer/api-interfaces";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Handler } from "./schema/handler.schema";

@Injectable({
    providedIn: "root",
})
export class HandlerService {
    constructor(@InjectModel(Handler.name) private HandlerModel: Model<Handler>) {}

    fetchHandlers(): Promise<Handler[]> {
        return this.HandlerModel.find().exec();
    }

    async createHandler(handlerDTO: HandlerDTO): Promise<Handler> {
        console.log("making handler with: ", handlerDTO);
        const handler = new this.HandlerModel(handlerDTO);
        return handler.save();
    }

    async bulkMakeHandlers(handlers: HandlerDTO[]): Promise<BulkCreateHandlerResponseDTO> {
        const savedHandlers: Promise<HandlerDTO>[] = handlers.map(async (handler: HandlerDTO) => {
            return await this.createHandler(handler);
        });
        const resolvedSavedHandlers: (Error | HandlerDTO)[] = await Promise.all(
            savedHandlers.map(savedHandler => savedHandler.catch(Error))
        );
        const nbSuccess = resolvedSavedHandlers.filter(mat => !(mat instanceof Error)).length;
        const errored = resolvedSavedHandlers.filter((mat): mat is Error => mat instanceof Error);

        return {
            success: !errored.length,
            nbHandlersSaved: nbSuccess,
            errorMessage: errored.map(error => `${error.message}\n${error.stack}`),
        };
    }
}
