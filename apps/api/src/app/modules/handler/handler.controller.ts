import type { BulkCreateHandlerPayloadDTO, BulkCreateHandlerResponseDTO, HandlerDTO } from "@disposer/api-interfaces";
import { Body, Controller, Get, Post } from "@nestjs/common";
import { HandlerService } from "./handler.service";
import { Handler } from "./schema/handler.schema";

@Controller("/handlers")
export class HandlerController {
    constructor(private handlerService: HandlerService) { }

    @Get()
    fetchHandlers(): Promise<Handler[]> {
        return this.handlerService.fetchHandlers();
    }

    @Post()
    makeBasicHandler(@Body() handlerDTO: HandlerDTO): Promise<Handler> {
        return this.handlerService.createHandler(handlerDTO);
    }

    @Post("/bulk")
    bulkMakeBasicHandler(@Body() bulkHandlerDTO: BulkCreateHandlerPayloadDTO): Promise<BulkCreateHandlerResponseDTO> {
        return this.handlerService.bulkMakeHandlers(bulkHandlerDTO.bulkHandlers);
    }
}
