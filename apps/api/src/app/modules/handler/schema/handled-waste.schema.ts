import { Condition } from "@disposer/api-interfaces";
import { Prop } from "@nestjs/mongoose";

// todo - extract somewhere perhaps
export interface Conditionable {
    condition?: Condition[];
}

export class HandledWasteMaterial implements Conditionable {
    @Prop()
    materialId: string;
    
    @Prop({ type: [String], lowercase: true, enum: Condition })
    condition?: Condition[];
}

export class HandledWasteGroup implements Conditionable {
    @Prop({ type: [String] })
    categoryIds: string[];

    @Prop({ type: [String], lowercase: true, enum: Condition })
    condition?: Condition[];
}

export class HandledWaste {
    @Prop()
    groups?: HandledWasteGroup[];

    @Prop()
    items?: HandledWasteMaterial[];

    @Prop()
    description?: string;
}

export class RejectedWaste extends HandledWaste {}
