import { CommentSource, ContactType, DisposalMethod, HandlerType as HandlerTypeEnum, Language } from "@disposer/api-interfaces";
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import { DLocation } from "../../../core/models/location.model";
import { HandledWaste, RejectedWaste } from "./handled-waste.schema";

export class HandlerType {
    @Prop({ required: true, type: Number, enum: Object.values(HandlerTypeEnum) })
    type: HandlerTypeEnum;

    @Prop({ type: [Number], enum: Object.values(DisposalMethod), default: [DisposalMethod.RECYCLED] })
    disposalMethods?: DisposalMethod[];

    @Prop({ default: true })
    managed: boolean;
}

export class Contact {
    @Prop({ required: true })
    contact: string;

    @Prop({ required: true, enum: Object.values(ContactType) })
    contactType: ContactType;
}

export class Comment {
    @Prop({ required: true })
    comment: string;

    @Prop({ enum: Object.values(CommentSource), default: CommentSource.USER })
    commentSource?: CommentSource;

    @Prop({ enum: Object.values(Language), default: Language.ENGLISH })
    commentLanguage?: Language;
}

@Schema()
export class Handler extends Document {
    @Prop({ required: true })
    name: string;

    @Prop({ required: true, type: DLocation })
    location: DLocation;

    @Prop()
    description: string;

    @Prop()
    website: string;

    @Prop()
    handlerType: HandlerType;

    @Prop({ required: false, type: HandledWaste })
    handledWaste?: HandledWaste;

    @Prop({ type: RejectedWaste })
    rejectedWaste?: RejectedWaste;

    @Prop({ type: () => [Contact] })
    contacts: Contact[];

    @Prop()
    comments?: Comment[];
}

export const HandlerSchema = SchemaFactory.createForClass(Handler);
