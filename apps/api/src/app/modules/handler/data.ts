// import { Condition } from "@disposer/api-interfaces";
// import { HandledWaste, RejectedWaste } from "./schema/handled-waste.schema";
// import { Handler } from "./schema/handler.schema";

// export const testingNewCategoryThing: HandledWaste = {
//     groups: [
//         {
//             categories: [
//                 { name: "Electronic", _id: "electronic" },
//                 { name: "Appliances", _id: "appliances" },
//             ],
//             condition: [Condition.NEW],
//         },
//         {
//             categories: [{ name: "Construction", _id: "construction" }],
//             condition: [Condition.ANY],
//         },
//     ],
//     items: [{ materialHandled: { _id: "metal-downpipe", name: "Metal downpipe", categories: [] } }],
// };

// const testingNewCategoryThingRejected: RejectedWaste = {
//     groups: [
//         {
//             categories: [
//                 { name: "Hazardous", _id: "hazardous" },
//                 { name: "Plastic", _id: "plastic" },
//             ],
//         },
//         {
//             categories: [{ name: "Glass", _id: "glass" }],
//             condition: [Condition.BROKEN],
//         },
//         {
//             categories: [{ name: "Paint", _id: "paint" }],
//         },
//     ],
//     items: [
//         { materialHandled: { _id: "oven", name: "Oven", categories: [] }, condition: [Condition.BROKEN] },
//         { materialHandled: { _id: "tarp", name: "Tarp", categories: [] } },
//         { materialHandled: { _id: "tiles", name: "Tiles", categories: [] } },
//     ],
//     description: "",
// };

// // what can we reject? we can reject for every category or combined category
// //      a series of exclusive categories
// //      a series of exclusive items

// export const mananthaDropOff: Handler = {
//     name: "mananthavady plastic drop off",
//     location: {
//         address: "gandhi park",
//         postcode: "670731",
//         landmark: "near bus stand",
//         photos: ["plastic_drop_off.jpg"],
//     },
//     handlerType: {
//         type: "Municipal recycling drop off",
//         meansOfDisposal: "sent to Bangalore for recycling", // + 10 points for this information
//         managed: false, // a dropdown can be given to add extra information
//     },
//     handledWaste: {
//         items: [
//             {
//                 materialHandled: {
//                     _id: "plastic-bottles",
//                     name: "Plastic bottles",
//                     description: "",
//                     categories: [
//                         { name: "Clear Plastic", _id: "clear-plastic" },
//                         { name: "Plastic #1", _id: "plastic-#1" }, // need help with this?
//                         { name: "Container", _id: "container" },
//                     ],
//                 },
//                 condition: [Condition.CLEAN],
//             },
//             {
//                 materialHandled: {
//                     _id: "plastic-tubs",
//                     name: "Plastic tubs",
//                     description: "Food grade container",
//                     categories: [
//                         { name: "Clear Plastic", _id: "clear-plastic" },
//                         { name: "Plastic #1", _id: "plastic-#1" }, // need help with this?
//                         { name: "Container", _id: "container" },
//                     ],
//                 },
//                 condition: [Condition.ANY],
//             },
//         ],
//     },
//     contacts: [],
//     comments: [{ comment: "If full you can keep on the side", commentSource: "HANDLER" }],
// };

// export const GreenWormsEcoCenter: Handler = {
//     name: "green worms eco center",
//     location: {
//         address: "outside calicut",
//         postcode: "677883",
//         landmark: "next to the big hardware shop",
//         photos: ["green_worms1.jpg", "green_worms2.jpg"],
//     },
//     handlerType: {
//         type: "Private waste center",
//         meansOfDisposal: "Recycles where possible",
//         managed: true,
//     },
//     handledWaste: {
//         groups: [
//             {
//                 categories: [{ name: "Plastic", _id: "plastic" }],
//             },
//             {
//                 categories: [{ name: "Electronics", _id: "electronics" }],
//             },
//             {
//                 categories: [{ name: "Metal", _id: "metal" }],
//             },
//             {
//                 categories: [{ name: "Glass", _id: "glass" }],
//             },
//         ],
//         description: "We accept almost all waste streams!",
//     },
//     rejectedWaste: {
//         items: [
//             // batteries is mostly likely a group
//             {
//                 materialHandled: {
//                     _id: "batteries",
//                     name: "Batteries",
//                     categories: [{ name: "Electronics", _id: "electronics" }],
//                 },
//                 condition: [Condition.ANY],
//             },
//         ],
//         description: "No batteries whatsoever please",
//     },
//     contacts: [{ contact: "+91 99614 57464", contactType: "WhatsApp" }],
//     comments: [{ comment: "Send a photo of your waste with details and they can come to collect", commentSource: "HANDLER" }],
// };

// // handler accepts scenarios:
// // all electrical appliances but not fridges
// // only plastic bottles
// // any construction materials or any metals but not broken glass
// /* #4:
//  - broken fridges, 
//  - dirty plastic wrappers, 
//  - electrical automative waste in good condition, 
//  - any bio waste but not moudly meat
// */

// // handledWasteCategories (& inclusive list)
// //      - can specify accepted or rejected conditions
// //      - rejected items in those categories (w/ condition?)
// // handledWasteItems (regardless of category)
// //      - (w/ condition)
// // <by default everything not mentioned is rejected>
// export const number4OnThatList: HandledWaste = {
//     groups: [
//         {
//             categories: [{ name: "Bio waste", _id: "bio-waste" }],
//             condition: [Condition.ANY],
//         },
//         {
//             categories: [
//                 { name: "Electrical", _id: "electrical" },
//                 { name: "Automative", _id: "automative" },
//             ],
//             condition: [Condition.GOOD],
//         },
//     ],
//     items: [
//         {
//             materialHandled: {
//                 _id: "fridges",
//                 name: "Fridges",
//                 categories: [
//                     { name: "Electrical", _id: "electrical" },
//                     { name: "Appliances", _id: "appliances" },
//                 ],
//             },
//             condition: [Condition.BROKEN],
//         },
//         {
//             materialHandled: {
//                 _id: "plastic-wrapper",
//                 name: "Plastic Wrapper",
//                 categories: [
//                     { name: "Packaging", _id: "packaging" },
//                     { name: "Plastic #3", _id: "plastic-#3" },
//                 ],
//             },
//             condition: [Condition.DIRTY],
//         },
//     ],
// };

// export const number4OnThatListRejected: RejectedWaste = {
//     items: [
//         {
//             materialHandled: { _id: "meat-scraps", name: "Meat Scraps", categories: [] },
//             condition: [Condition.MOULDY, Condition.DIRTY],
//         },
//     ],
// };

// export const handlerOfElectricalAppliancesButNotFridges: HandledWaste = {
//     groups: [
//         {
//             categories: [
//                 { name: "Electronics", _id: "electronics" },
//                 { name: "Appliances", _id: "appliances" },
//             ],
//         },
//         {
//             categories: [{ name: "Metal Scrap", _id: "metal-scrap" }],
//         },
//     ],
// };

// export const handlerOfOnlyPlasticBottlesAndContainers: HandledWaste = {
//     items: [
//         { materialHandled: { _id: "plastic-bottles", name: "Plastic bottles", categories: [] } },
//         { materialHandled: { _id: "plastic-containers", name: "Plastic Containers", categories: [] } },
//     ],
// };

// export const handlerOfConstructionMatsAndMetalsButNoBrokenGlass: Handler = {
//     name: "bob the builder scrap",
//     location: {
//         address: "outside bangalore",
//         postcode: "677883",
//         landmark: "next to the highway",
//         photos: ["bob_scraps1.jpg", "bob_scraps2.jpg"],
//     },
//     handlerType: {
//         type: "Private dumping ground",
//         meansOfDisposal: "Repurposes mats",
//         managed: true,
//     },
//     handledWaste: {
//         groups: [
//             {
//                 categories: [{ name: "Construction", _id: "construction" }],
//                 condition: [Condition.NEW, Condition.CLEAN],
//             },
//             {
//                 categories: [{ name: "Metal", _id: "metal" }],
//             },
//         ],
//     },
//     rejectedWaste: {
//         groups: [
//             {
//                 categories: [{ name: "Glass", _id: "glass" }],
//                 condition: [Condition.BROKEN],
//             },
//         ],
//     },
//     contacts: [{ contact: "+91 99614 57464", contactType: "WhatsApp" }],
//     comments: [{ comment: "Send a photo of your waste with details and they can come to collect", commentSource: "HANDLER" }],
// };
