import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { MaterialController } from "./material.controller";
import { MaterialService } from "./material.service";
import { Category, CategorySchema } from "./schema/category.schema";
import { Material, MaterialSchema } from "./schema/material.schema";

@Module({
    imports: [MongooseModule.forFeature([
        {name: Material.name, schema: MaterialSchema}, 
        {name: Category.name, schema: CategorySchema},
    ])],
    controllers: [MaterialController],
    providers: [MaterialService],
    exports: [MongooseModule] // will this be enough?
})
export class MaterialModule {
}
