import type { BulkCreateMaterialPayloadDTO, BulkCreateMaterialResponseDTO, CategoryDTO, MaterialDTO } from "@disposer/api-interfaces";
import { Body, Controller, Get, Post } from "@nestjs/common";
import { MaterialService } from "./material.service";
import { Category } from "./schema/category.schema";
import { Material } from "./schema/material.schema";

@Controller("/materials")
export class MaterialController {
    constructor(private materialService: MaterialService) {}

    @Get()
    fetchMaterials(): Promise<Material[]> {
        return this.materialService.fetchMaterials();
    }

    @Post()
    createMaterial(@Body() materialFields: MaterialDTO): Promise<Material> {
        return this.materialService.createMaterial(materialFields);
    }

    @Post("/bulk")
    bulkCreateMaterial(@Body() bulkMaterialsPayload: BulkCreateMaterialPayloadDTO): Promise<BulkCreateMaterialResponseDTO> {
        return this.materialService.bulkCreateMaterial(bulkMaterialsPayload.bulkMaterials);
    }

    @Get("/categories")
    fetchCategories(): Promise<Category[]> {
        return this.materialService.fetchCategories();
    }

    @Post("/categories")
    createCategory(@Body() category: CategoryDTO) {
        return this.materialService.findOrCreateCategory(category);
    }
}
