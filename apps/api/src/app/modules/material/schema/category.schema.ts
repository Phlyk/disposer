import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema({ _id: false })
export class Category extends Document {
    @Prop({ required: true })
    _id: string;

    @Prop({ required: true, unique: true })
    name: string;
}

export const CategorySchema = SchemaFactory.createForClass(Category);
