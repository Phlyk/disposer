import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import { Category } from "./category.schema";

@Schema({ _id: false })
export class Material extends Document {
    @Prop({ required: true })
    _id!: string;

    @Prop({ required: true, unique: true })
    name!: string;

    @Prop({ required: true, type: () => [Category], default: [] })
    categories!: Category[];

    @Prop()
    description?: string;
}

export const MaterialSchema = SchemaFactory.createForClass(Material);
