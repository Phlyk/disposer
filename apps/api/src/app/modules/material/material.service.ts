import { BulkCreateMaterialResponseDTO, CategoryDTO, MaterialDTO } from "@disposer/api-interfaces";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Category } from "./schema/category.schema";
import { Material } from "./schema/material.schema";

@Injectable()
export class MaterialService {
    constructor(
        @InjectModel(Material.name) private readonly MaterialModel: Model<Material>,
        @InjectModel(Category.name) private readonly CategoryModel: Model<Category>
    ) {}

    fetchMaterials(): Promise<Material[]> {
        return this.MaterialModel.find().sort("name").exec();
    }

    fetchCategories(): Promise<Category[]> {
        return this.CategoryModel.find().sort("name").exec();
    }

    async createMaterial(materialDto: MaterialDTO): Promise<Material> {
        const categoryPromises: Promise<Category>[] = materialDto.categories.map(category => this.findOrCreateCategory(category));
        return Promise.all(categoryPromises).then((categories: Category[]) => this.findOrCreateMaterial(materialDto, categories));
    }

    async bulkCreateMaterial(materials: MaterialDTO[]): Promise<BulkCreateMaterialResponseDTO> {
        const savedMaterials: Promise<MaterialDTO>[] = materials.map(async (material: MaterialDTO) => {
            return await this.createMaterial(material);
        });
        const resolvedSavedMaterials: (Error | MaterialDTO)[] = await Promise.all(
            savedMaterials.map(savedMaterial => savedMaterial.catch(Error))
        );
        const nbSuccess = resolvedSavedMaterials.filter(mat => !(mat instanceof Error)).length;
        const errored = resolvedSavedMaterials.filter((mat): mat is Error => mat instanceof Error);

        return {
            success: !errored.length,
            nbMaterialsSaved: nbSuccess,
            errorMessage: errored.map(error => `${error.message}\n${error.stack}`),
        };
    }

    async findOrCreateCategory(category: CategoryDTO): Promise<Category> {
        const idSlug = slugify(category.name);
        return await this.CategoryModel.findOneAndUpdate(
            { _id: idSlug },
            { $setOnInsert: { _id: idSlug, name: category.name } },
            { new: true, upsert: true }
        ).exec();
    }

    private async findOrCreateMaterial(materialDto: MaterialDTO, categories: Category[]) {
        const idSlug = slugify(materialDto.name);
        return await this.MaterialModel.findOneAndUpdate(
            { _id: idSlug },
            { $setOnInsert: { _id: idSlug, ...materialDto, categories } },
            { new: true, upsert: true }
        ).exec();
    }
}

function slugify(originalString: string): string {
    return originalString
        .toString()
        .trim()
        .toLowerCase()
        .replace(/\s+/g, "-")
        .replace(/[^\w\-]+/g, "")
        .replace(/\-\-+/g, "-")
        .replace(/^-+/, "")
        .replace(/-+$/, "");
}
