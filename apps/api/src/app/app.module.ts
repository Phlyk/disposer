import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { AppLoggerMiddleware } from "./core/middleware/request-logger.middleware";
import { HandlerModule } from "./modules/handler/handler.module";
import { MaterialModule } from "./modules/material/material.module";


@Module({
    imports: [
        MongooseModule.forRoot("mongodb://localhost:27017", {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            dbName: "disposer",
            useFindAndModify: false,
            useCreateIndex: true,
        }),
        HandlerModule, 
        HandlerModule,
        MaterialModule,
    ]
})
export class AppModule implements NestModule { 
    constructor() {
        mongoose.set('debug', true);
    }
    
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(AppLoggerMiddleware).forRoutes("*");
    }
}
