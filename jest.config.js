module.exports = {
    projects: [
        "<rootDir>/apps/disposer-app",
        "<rootDir>/apps/api",
        "<rootDir>/libs/api-interfaces",
        "<rootDir>/libs/common",
        "<rootDir>/libs/components",
    ],
};
