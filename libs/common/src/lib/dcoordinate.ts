export type DCoordinates = {
    lat: number;
    long: number;
};
