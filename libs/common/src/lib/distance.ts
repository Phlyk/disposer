export type Distance = {
    distance: Number,
    unit: "miles" | "km"
}
