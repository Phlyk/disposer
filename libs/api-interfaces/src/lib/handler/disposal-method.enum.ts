export enum DisposalMethod {
    LANDFILL,
    REUSE,
    RECYCLED,
    WASTE_ENERGY,
}
