import { Condition } from "../common";

export class HandledWasteDTO {
    groups?: HandledWasteGroupDTO[];
    items?: HandledWasteMaterialDTO[];
    description?: string;
}

export class HandledWasteGroupDTO {
    categoryIds: string[];
    condition?: Condition[];
}

export class HandledWasteMaterialDTO {
    materialId: string;
    condition?: Condition[];
}
