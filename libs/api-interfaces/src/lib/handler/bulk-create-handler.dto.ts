import { HandlerDTO } from "./handler.dto";

export interface BulkCreateHandlerResponseDTO {
    success: boolean;
    nbHandlersSaved: number;
    errorMessage?: any;
}

export interface BulkCreateHandlerPayloadDTO {
    readonly bulkHandlers: HandlerDTO[];
}
