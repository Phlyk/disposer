import { DisposalMethod } from "./disposal-method.enum";
import { HandlerType } from "./handler-type.enum";

export class HandlerTypeDTO {
    type: HandlerType;
    disposalMethods?: DisposalMethod[];
    managed: boolean;
}
