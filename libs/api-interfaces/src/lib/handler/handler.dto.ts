import { CommentDTO } from "../common";
import { ContactDTO } from "../common/contact-type.dto";
import { DLocationDTO } from "../common/location.dto";
import { HandledWasteDTO } from "./handled-waste.dto";
import { HandlerTypeDTO } from "./handler-type.dto";

export class HandlerDTO {
    _id: string;
    name!: string;
    location!: DLocationDTO;
    description?: string;
    website?: string;
    handlerType: HandlerTypeDTO;
    handledWaste?: HandledWasteDTO;
    rejectedWaste?: HandledWasteDTO;
    contacts?: ContactDTO[];
    comments?: CommentDTO[];
}
