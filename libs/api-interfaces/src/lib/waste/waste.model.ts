// // import { WasteCategory, WasteSubType } from '../common/waste-types.type';

// export interface WasteItem {
//     name: string;
//     category: WasteCategory;
//     subType: WasteSubType;
//     quantity: Quantity;
//     details?: WasteDetails;
// }

export class Quantity {
    size?: ItemDimensions;
    weight?: number;
    number?: number;
}

// export interface WasteDetails {
//     description?: string; 
//     extraComments?: string;
//     condition?: ItemCondition;
// }

// export type ItemCondition = "NEW" | "USED" | "BAD";

export interface ItemDimensions {
    height: number;
    width: number;
    bredth: number;
}
