import { LocationType } from "./location-type.enum";

export interface DLocationDTO {
    address: string;
    city: string;
    state: string;
    country?: string;
    postcode: string;
    landmark?: string;
    locationType?: LocationType;
    latitude: number;
    longitude: number;
}
