export enum ContactType {
    WA = "whatsapp",
    EMAIL = "email",
    PHONE = "phone",
}
