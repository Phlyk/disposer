export enum Condition {
    ANY = "any",
    OLD = "old",
    NEW = "new",
    BROKEN = "broken",
    CLEAN = "clean",
    DIRTY = "dirty",
    MOULDY = "mouldy",
    GOOD = "good"                                                                                                                                
}
