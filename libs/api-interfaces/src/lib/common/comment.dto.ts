import { CommentSource } from "./comment-source.enum";
import { Language } from "./language.enum";

export class CommentDTO {
    comment: string;
    commentSource?: CommentSource;
    commentLanguage?: Language;
}
