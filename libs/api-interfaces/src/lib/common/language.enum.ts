export enum Language {
    ENGLISH = "en",
    HINDI = "hi",
    MALAYALAM = "ml",
}
