import { ContactType } from "./contact-type.enum";

export class ContactDTO {
    contact: string;
    contactType: ContactType;
}
