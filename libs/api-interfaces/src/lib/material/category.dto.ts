export interface CategoryDTO {
    readonly _id: string;
    readonly name: string;
}
