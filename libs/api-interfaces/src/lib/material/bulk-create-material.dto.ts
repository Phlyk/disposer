import { MaterialDTO } from "./material.dto";

export interface BulkCreateMaterialResponseDTO {
    success: boolean;
    nbMaterialsSaved: number;
    errorMessage?: any;
}

export interface BulkCreateMaterialPayloadDTO {
    readonly bulkMaterials: MaterialDTO[];
}
