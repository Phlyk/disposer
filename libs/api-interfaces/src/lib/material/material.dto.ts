import { CategoryDTO } from "./category.dto";

export interface MaterialDTO {
    readonly _id: string;
    
    readonly name: string;

    readonly categories: CategoryDTO[];

    readonly description?: string;
}
