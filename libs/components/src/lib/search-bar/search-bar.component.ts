import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

@Component({
    selector: "disposer-search-bar",
    templateUrl: "./search-bar.component.html",
    styleUrls: ["./search-bar.component.scss"],
})
export class SearchBarComponent implements OnInit {
    @Input() 
    placeholder = "Search away...";

    @Output() 
    private searchBarInput = new EventEmitter<string>();

    form: FormGroup;
    @ViewChild("input") input: ElementRef;

    ngOnInit() {
        const queryFormControl = new FormControl("");
        this.form = new FormGroup({
            query: queryFormControl,
        });

        queryFormControl.valueChanges
            .pipe(debounceTime(400), distinctUntilChanged())
            .subscribe((searchInputText: string) => this.searchBarInput.emit(searchInputText));
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.input.nativeElement.focus();
        }, 400);
    }
}
