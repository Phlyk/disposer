import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { SearchBarComponent } from "./search-bar.component";

@NgModule({
    declarations: [SearchBarComponent],
    imports: [CommonModule, IonicModule],
    exports: [SearchBarComponent],
})
export class FeatureModule {}
