import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { SlidingItemComponent } from "./sliding-item.component";

@NgModule({
    declarations: [SlidingItemComponent],
    imports: [CommonModule, IonicModule],
    exports: [SlidingItemComponent],
})
export class SlidingItemModule {}
