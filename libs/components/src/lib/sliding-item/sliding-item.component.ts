import { Component, Input } from "@angular/core";

@Component({
    selector: "disposer-sliding-item",
    templateUrl: "./sliding-item.component.html",
    styleUrls: ["./sliding-item.component.scss"],
})
export class SlidingItemComponent {
    @Input() leftSideLabel = "";
    @Input() rightSideLabel = "";
}
